'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: {
      // Configurable paths
      assets: 'assets/final',
      dist: 'dist',
      tmp: '.tmp'
    },

    // Generates various images for app use
    responsive_images: {
      options: { engine: 'im' },

      iIcons: {
        options: {
          sizes: [{
            width: 512,
            height: 512,
            aspectRatio: false
          }, {
            width: 114,
            height: 114,
            aspectRatio: false
          }, {
            width: 96,
            height: 96,
            aspectRatio: false
          }, {
            width: 72,
            height: 72,
            aspectRatio: false
          }]
        },
        files: [{
          expand: true,
          src: 'icon-ios.png',
          cwd: '<%= config.assets %>/',
          dest: '<%= config.tmp %>/'
        }]
      },

      aIcons: {
        options: {
          sizes: [{
            width: 512,
            height: 512,
            aspectRatio: false
          }, {
            width: 96,
            height: 96,
            aspectRatio: false
          }]
        },
        files: [{
          expand: true,
          src: 'icon-android.png',
          cwd: '<%= config.assets %>/',
          dest: '<%= config.tmp %>/'
        }]
      },

      aSplash: {
        options: {
          sizes: [{
            width: 1280,
            height: 800,
            aspectRatio: false
          }, {
            width: 800,
            height: 480,
            aspectRatio: false
          }]
        },
        files: [{
          expand: true,
          src: 'splash.png',
          cwd: '<%= config.assets %>/',
          dest: '<%= config.tmp %>/'
        }]
      },

      iSplash: {
        options: {
          sizes: [{
            width: 1024,
            height: 768,
            aspectRatio: false
          }, {
            width: 960,
            height: 640,
            aspectRatio: false
          }]
        },
        files: [{
          expand: true,
          src: 'splash.png',
          cwd: '<%= config.assets %>/',
          dest: '<%= config.tmp %>/'
        }]
      },

        iBg: {
          options: {
          sizes: [{
            width: 1024,
            height: 1024,
            aspectRatio: false
          }, {
            width: 960,
            height: 960,
            aspectRatio: false
          }]
        },
        files: [{
          expand: true,
          src: 'bg.png',
          cwd: '<%= config.assets %>/',
          dest: '<%= config.tmp %>/'
        }]
      },

    },

    // Create zip archives for each platform
    compress: {
      android: {
        options: {
          archive: '<%= config.dist %>/android_assets.zip'
        },
        files: [{
          expand: true,
          src: 'android_assets/*',
          cwd: '<%= config.tmp %>/',
          dest: ''
        }]
      },
      ios: {
        options: {
          archive: '<%= config.dist %>/ios_assets.zip'
        },
        files: [{
          expand: true,
          src: 'ios_assets/*',
          cwd: '<%= config.tmp %>/',
          dest: ''
        }]
      }
    },
      
    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.tmp %>/**',
            '<%= config.tmp %>',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      images: {
        files: [{
          src: [
            '<%= config.tmp %>/icon-*.png',
            '<%= config.tmp %>/splash-*.png',
            '<%= config.tmp %>/bg-*.png'
          ]
        }]
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    copy: {
      android: {
        expand: true,
        cwd: '<%= config.tmp %>',
        dest: '<%= config.tmp %>/android_assets/',
        src: ['*.png']
      },
      ios: {
        expand: true,
        cwd: '<%= config.tmp %>',
        dest: '<%= config.tmp %>/ios_assets/',
        src: ['*.png']
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js'
      ]
    },

    concurrent: {
      android: [
        'responsive_images:aIcons',
        'responsive_images:aSplash'
      ],
      ios: [
        'responsive_images:iIcons',
        'responsive_images:iSplash',
        'responsive_images:iBg'
      ]
    }

  });

  grunt.registerTask('build', [
    'jshint',
    'clean:dist',
    'responsive_images:aIcons',
    'responsive_images:aSplash',
    'copy:android',
    'clean:images',
    'responsive_images:iIcons',
    'responsive_images:iSplash',
    'responsive_images:iBg',
    'copy:ios',
    'clean:images',
    'compress'
  ]);

};

