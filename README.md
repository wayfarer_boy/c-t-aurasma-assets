C&T: Aurasma assets
===================

A Grunt Aurasma app skin-builder
--------------------------------

### Installation

You'll need to have installed [nodejs](http://nodejs.org/download/), then to fetch the node modules required for building the zip archives, run `npm install` from the project directory.

### ImageMagick
Make sure ImageMagick is installed on your system and properly set up in your `PATH`.

Ubuntu:

```shell
$ apt-get install imagemagick
```

Mac OS X (using [Homebrew](http://brew.sh/)):

```shell
$ brew install imagemagick
```

Windows & others: 

[http://www.imagemagick.org/script/binary-releases.php](http://www.imagemagick.org/script/binary-releases.php)

Confirm that ImageMagick is properly set up by executing `convert -help` in a terminal.

### Artwork preparation

To build the required zip files for both Android and iOS Aurasma applications, you need to prepare the artwork under the following guidelines:

#### Icon

The icon should be a square png no smaller than 512x512, and should have the following filenames:

* `assets/final/icon-ios.png` iOS icon (squared, with no transparent areas)
* `assets/final/icon-android.png` Android icon (no limitations to design)

#### Splash

The splash image should be a rectangular png no smaller than 2560x1920. It should also be designed to fit the various screen aspect ratios. Use `assets/final/overscan.png` as a guide when designing the splash image.

The final image should be saved as `assets/final/splash.png`.

#### Background

The iOS Aurasma skinned apps also accept a background image for use in the main interface. The square png should be no smaller than 2048x2048, and should be saved as `assets/final/ios-bg.png`.

### Building

Once all files are in place, run `grunt build` and once Grunt has done it's thang, your zip files can be found in the `dist` directory.

